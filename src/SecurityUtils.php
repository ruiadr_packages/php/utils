<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Interface\SecurityUtilsInterface;

final class SecurityUtils implements SecurityUtilsInterface
{
    public static function randomString(string $bytes, int $size = 16): string
    {
        if ($size < 1) {
            $size = 1;
        }

        $bytes = StringUtils::shuffle($bytes);
        $length = strlen($bytes) - 1;
        $result = '';

        for ($i = 0; $i < $size; ++$i) {
            // "random_int" est sécurisé contrairement à la fonction "rand".
            $result .= $bytes[random_int(0, $length)];
        }

        return $result;
    }

    public static function randomAlphanum(int $size = 16, bool $withUppercase = true): string
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyz';
        $uppercaseAlphabet = $withUppercase ? strtoupper($alphabet) : '';
        $numbers = '0123456789';

        return self::randomString($alphabet.$uppercaseAlphabet.$numbers, $size);
    }
}
