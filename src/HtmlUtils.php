<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Interface\HtmlUtilsInterface;

final class HtmlUtils implements HtmlUtilsInterface
{
    public static function sanitizeHTML(string $html, string $allowedTags = ''): string
    {
        $html = strip_tags($html, $allowedTags);
        $html = str_replace(['&nbsp;', '&gt;', '&lt;'], ' ', $html);
        $html = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si", '<$1$2>', $html);

        return trim($html);
    }
}
