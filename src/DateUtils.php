<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Exception\DateWrongFormatException;
use Ruiadr\Utils\Interface\DateUtilsInterface;

final class DateUtils implements DateUtilsInterface
{
    public static function stringToDatetime(
        string $dateTime, string $format = DateUtilsInterface::DEFAULT_FORMAT
    ): \DateTimeImmutable {
        $dateTimeImmutable = \DateTimeImmutable::createFromFormat($format, $dateTime);
        if (false === $dateTimeImmutable) {
            throw new DateWrongFormatException('Wrong date format');
        }

        return $dateTimeImmutable;
    }
}
