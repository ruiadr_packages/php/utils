<?php

namespace Ruiadr\Utils\Interface;

use Ruiadr\Utils\Exception\DateWrongFormatException;

interface DateUtilsInterface
{
    public const FORMAT_SLASH_DDMMYYYY = 'd/m/Y';
    public const FORMAT_SLASH_MMDDYYYY = 'm/d/Y';
    public const FORMAT_HYPHEN_YYYYMMDD = 'Y-m-d';
    public const DEFAULT_FORMAT = self::FORMAT_SLASH_DDMMYYYY;

    /**
     * Retourne un objet de type \DateTimeImmutable créé à partir des
     * chaînes passées en paramètre. La date $dateTime doit être au format
     * $format pour fonctionner sans quoi une DateWrongFormatException
     * peut être levée.
     *
     * @param string $dateTime Date au format texte
     * @param string $format   Format de la date de $dateTime
     *
     * @return \DateTimeImmutable Objet date lorsque les paramètres sont cohérents
     *
     * @throws \ValueError              Levée si $dateTime contient des NULL-bytes
     * @throws DateWrongFormatException Levée si la conversion échoue
     */
    public static function stringToDatetime(
        string $dateTime, string $format = DateUtilsInterface::DEFAULT_FORMAT
    ): \DateTimeImmutable;
}
