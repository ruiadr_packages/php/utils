<?php

namespace Ruiadr\Utils\Interface;

interface HtmlUtilsInterface
{
    /**
     * Nettoyage du HTML qui est contenu dans le paramètre $html.
     * Possibilité de conserver les balises qui se trouvent dans la chaîne $allowedTags.
     * Cette méthode s'appuie sur la fonction PHP "strip_tags", il faut donc utiliser
     * $allowedTags de la même manière, par exemple: "<p><span>" pour conserver
     * les balises "p" et "span".
     *
     * @param string $html        Contenu au format HTML
     * @param string $allowedTags Liste des tags autorisés en sortie
     *
     * @return string Chaîne nettoyée
     */
    public static function sanitizeHTML(string $html, string $allowedTags = ''): string;
}
