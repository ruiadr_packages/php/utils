<?php

namespace Ruiadr\Utils\Interface;

interface StringUtilsInterface
{
    /**
     * Supprime les espaces en début et fin de chaîne et la
     * retourne en minuscule.
     *
     * @param string $value La chaîne à traiter
     *
     * @return string La valeur traitée
     */
    public static function lowerTrim(string $value): string;

    /**
     * Supprime les espaces en début et fin de chaîne et la
     * retourne en majuscule.
     *
     * @param string $value La chaîne à traiter
     *
     * @return string La valeur traitée
     */
    public static function upperTrim(string $value): string;

    /**
     * Met en majuscule la première lettre de la chaîne $name passée
     * en paramètre. Tous les autres caractères sont mis en minuscules.
     * Les espaces qui se trouvent avant et après $name sont retirés.
     *
     * @param string $value La chaîne sur laquelle réaliser les modifications
     *
     * @return string La chaîne retravaillée
     */
    public static function capitalize(string $value): string;

    /**
     * Retire de la chaîne $value passée en paramètre tous les caractères
     * qui ne sont pas alphanumériques.
     *
     * @param string $value Chaîne à traiter
     *
     * @return string Chaîne traitée
     */
    public static function sanitizeAlnum(string $value): string;

    /**
     * Retire les caractères spéciaux de la chaîne $string passée en paramètre.
     *
     * @param string $string Chaîne sur laquelle retirer les caractères spéciaux
     *
     * @return string Chaîne nettoyée
     */
    public static function replaceSpecialChars(string $string): string;

    /**
     * Mélange les caractères d'une chaîne de caractères.
     * Si la version de PHP >= 8.2.0, la méthode utilisera automatiquement le shuffle
     * de la classe Randomizer pour réaliser un mélange sécurisé.
     * Possibilité d'utiliser un shuffle plus classique et moins sécurisé en passant
     * le paramètre $forceInsecure à true.
     *
     * @param string $bytes         Chaîne à mélanger
     * @param bool   $forceInsecure Utiliser un shuffle classique mais moins sécurisé
     *
     * @return string Chaîne mélangée
     */
    public static function shuffle(string $bytes, bool $forceInsecure = false): string;
}
