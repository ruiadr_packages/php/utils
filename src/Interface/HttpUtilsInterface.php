<?php

namespace Ruiadr\Utils\Interface;

interface HttpUtilsInterface
{
    /**
     * Retourne une chaîne nettoyée qui correspond au domaine de l'url $url
     * passée en paramètre. En cas de problème, la méthode retourne null.
     *
     * @param string $url Url à nettoyer
     *
     * @return string Nom de domaine valide récupéré de $url
     */
    public static function sanitizeDomain(string $url): ?string;

    /**
     * Retourne true si une des valeurs du tableau $list passé en paramètre
     * correspond bien à un site référent de $_SERVER['HTTP_REFERER'].
     *
     * ['mondomaine.com', 'mondomaine.net', 'mondomaine.io'] pour mondomaine.fr = false
     * ['mondomaine.com', 'mondomaine.net', 'mondomaine.io'] pour mondomaine.io = true
     *
     * La méthode assure la validité du domaine référent qui se trouve dans $_SERVER['HTTP_REFERER'].
     *
     * @param array $list Liste de domaines à comparer au referrer courant
     *
     * @return bool true si au moins un élément de la liste coïncide bien avec le referrer courant
     */
    public static function isValidReferer(array $list): bool;
}
