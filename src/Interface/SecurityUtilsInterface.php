<?php

namespace Ruiadr\Utils\Interface;

interface SecurityUtilsInterface
{
    /**
     * Génère de manière aléatoire une chaîne en partant des caractères contenus dans
     * le paramètre $bytes, et de longueur $size. Le paramètre $size doit être >= 1.
     *
     * La méthode retourne une chaîne dont le caractère aléatoire est sécurisé pour
     * les versions de PHP >= 8.2.0.
     *
     * @param string $bytes Les caractères sur lesquels baser la génération aléatoire
     * @param int    $size  La longueur de la chaîne à retourner
     *
     * @return string La chaîne générée aléatoirement
     */
    public static function randomString(string $bytes, int $size = 16): string;

    /**
     * Génère une chaîne alphanumérique aléatoire.
     * Il est possible d'exclure les majuscules en passant le second
     * paramètre $withUppercase à false.
     *
     * @param int  $size          La longueur de la chaîne à générer aléatoirement
     * @param bool $withUppercase true ou false pour inclure des majuscules dans le résultat
     *
     * @return string La chaîne générée aléatoirement
     */
    public static function randomAlphanum(int $size = 16, bool $withUppercase = true): string;
}
