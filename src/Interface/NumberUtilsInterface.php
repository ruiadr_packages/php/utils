<?php

namespace Ruiadr\Utils\Interface;

interface NumberUtilsInterface
{
    /**
     * Retourne le pourcentage entre $value et $max.
     * $value étant la valeur de référence, et $max la valeur maximale.
     *
     * @param float $value Valeur de référence pour le calcul
     * @param float $max   Valeur max pour le calcul
     *
     * @return float Le pourcentage calculé
     */
    public static function percent(float $value, float $max): float;
}
