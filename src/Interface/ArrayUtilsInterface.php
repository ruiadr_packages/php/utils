<?php

namespace Ruiadr\Utils\Interface;

interface ArrayUtilsInterface
{
    /**
     * Réalise un merge récursif de tous les tableaux qui se trouvent dans $arrays passé en paramètre.
     *
     * [
     *      ['a' => 1, 'b' => 3, 'd' => ['aa' => 1, 'bb' => 3]]]
     *      ['b' => 2, 'c' => 3]
     *      ['d' => 4, 'd' => ['bb' => 2]]
     * ]
     *
     * Retourne ['a' => 1, 'b' => 2, 'd' => ['aa' => 1, 'bb' => 2], 'c' => 3, 'd' => 4]
     *
     * Le paramètre $preserveKeys permet de conserver l'index lorsqu'il s'agit d'un entier.
     *
     * [
     *      ['a', 'b']
     *      ['c']
     * ]
     *
     * Avec $preserveKeys === false, retourne ['a', 'b', 'c']
     * Avec $preserveKeys === true, retourne ['c', 'b']
     *
     * @param array $arrays       Tableau de tableaux à merger
     * @param bool  $preserveKeys Si les clés du tableau final doivent être conservées
     *
     * @return array Tableau résultant du merge
     */
    public static function deepMergeArray(array $arrays, bool $preserveKeys = false): array;

    /**
     * Permet d'utiliser la méthode deepMergeArray mais en passant
     * un tableau par paramètre, exemple:
     *
     *     $a1 = ['a' => 1];
     *     $a2 = ['b' => 3];
     *     $a3 = ['c' => '3', 'b' => 2];
     *
     *     $a = ArrayUtils::deepMerge($a1, $a2, $a3);
     *
     *     Résultat: $a contient ['a' => 1, 'b' => 2, 'c' => 3]
     *
     * @return array Tableau résultant du merge
     */
    public static function deepMerge(): array;
}
