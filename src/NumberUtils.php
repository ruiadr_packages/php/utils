<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Interface\NumberUtilsInterface;

final class NumberUtils implements NumberUtilsInterface
{
    public static function percent(float $value, float $max): float
    {
        return round(100.0 - ($value * 100.0 / $max), 2);
    }
}
