<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Interface\ArrayUtilsInterface;

final class ArrayUtils implements ArrayUtilsInterface
{
    public static function deepMergeArray(array $arrays, bool $preserveKeys = false): array
    {
        $result = [];

        foreach ($arrays as $array) {
            foreach ($array as $key => $value) {
                if (is_int($key) && !$preserveKeys) {
                    $result[] = $value;
                } elseif (isset($result[$key]) && is_array($result[$key]) && is_array($value)) {
                    $result[$key] = self::deepMergeArray([$result[$key], $value], $preserveKeys);
                } else {
                    $result[$key] = $value;
                }
            }
        }

        return $result;
    }

    public static function deepMerge(): array
    {
        return self::deepMergeArray(func_get_args());
    }
}
