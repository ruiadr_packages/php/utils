<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Interface\StringUtilsInterface;

final class StringUtils implements StringUtilsInterface
{
    public static function lowerTrim(string $value): string
    {
        return strtolower(trim($value));
    }

    public static function upperTrim(string $value): string
    {
        return strtoupper(trim($value));
    }

    public static function capitalize(string $value): string
    {
        return ucfirst(self::lowerTrim($value));
    }

    public static function sanitizeAlnum(string $value): string
    {
        return preg_replace('/[^[:alnum:]]/s', '', trim($value));
    }

    public static function replaceSpecialChars(string $string): string
    {
        $string = htmlentities($string, ENT_NOQUOTES, 'utf-8');
        $string = preg_replace([
            '#&([A-Za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#',
            '#&([A-Za-z]{2});#',
            '#&[^;]+;#',
        ], '\1', $string);

        return $string;
    }

    public static function shuffle(string $bytes, bool $forceInsecure = false): string
    {
        if (!$forceInsecure && PHP_VERSION_ID >= 80200) {
            // Shuffle plus sécurisé, à condition d'être sur la version min de PHP 8.2.0.
            $bytes = (new \Random\Randomizer())->shuffleBytes($bytes);
        } else {
            $bytes = str_shuffle($bytes);
        }

        return $bytes;
    }
}
