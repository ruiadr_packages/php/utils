<?php

namespace Ruiadr\Utils;

use Ruiadr\Utils\Interface\HttpUtilsInterface;

final class HttpUtils implements HttpUtilsInterface
{
    public static function sanitizeDomain(string $url): ?string
    {
        $parsed = parse_url($url);
        if (isset($parsed['scheme'], $parsed['host'])) {
            return $parsed['scheme'].'://'.$parsed['host'];
        }

        return null;
    }

    public static function isValidReferer(array $list): bool
    {
        $httpReferer = $_SERVER['HTTP_REFERER'] ?? null;
        if (null === $httpReferer) {
            return false;
        }

        $source = self::sanitizeDomain(trim($httpReferer));
        if (is_string($source)) {
            foreach ($list as $entry) {
                if (preg_match("/$entry/", $source)) {
                    return true;
                }
            }
        }

        return false;
    }
}
