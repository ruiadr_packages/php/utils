<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\HttpUtils;

final class HttpUtilsTest extends TestCase
{
    private const PROTOCOL = 'https';
    private const SUBDOMAIN = 'www.adrien-ruiz.fr';
    private const URL = self::PROTOCOL.'://'.self::SUBDOMAIN.'/test.html?a=1';
    private const DOMAIN_COM = 'adrien-ruiz.com';
    private const DOMAIN_NET = 'adrien-ruiz.net';
    private const DOMAIN_IO = 'adrien-ruiz.io';
    private const DOMAIN_FR = 'adrien-ruiz.fr';

    private function appendReferrer(?string $url = self::URL): void
    {
        if (is_string($url)) {
            $_SERVER['HTTP_REFERER'] = $url;
        } elseif (null === $url) {
            unset($_SERVER['HTTP_REFERER']);
        }
    }

    public function testSanitizeDomain(): void
    {
        $resultTest = HttpUtils::sanitizeDomain(self::URL);

        $this->assertSame(self::PROTOCOL.'://'.self::SUBDOMAIN, $resultTest);
        $this->assertNull(HttpUtils::sanitizeDomain(''));
    }

    public function testWrongReferrer(): void
    {
        $this->appendReferrer();

        $resultTest = HttpUtils::isValidReferer([
            self::DOMAIN_COM, self::DOMAIN_NET, self::DOMAIN_IO,
        ]);

        $this->assertFalse($resultTest);
    }

    public function testRefererNotSet(): void
    {
        $this->appendReferrer(null);

        $resultTest = HttpUtils::isValidReferer([self::DOMAIN_COM]);

        $this->assertFalse($resultTest);
    }

    public function testValidReferrer(): void
    {
        $this->appendReferrer();

        $resultTest = HttpUtils::isValidReferer([
            self::DOMAIN_COM, self::DOMAIN_FR, self::DOMAIN_NET,
        ]);

        $this->assertTrue($resultTest);
    }
}
