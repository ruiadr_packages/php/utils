<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\DateUtils;
use Ruiadr\Utils\Exception\DateWrongFormatException;
use Ruiadr\Utils\Interface\DateUtilsInterface;

final class DateUtilsTest extends TestCase
{
    private function isException(string $date, string $format): bool
    {
        $isException = false;

        try {
            DateUtils::stringToDatetime($date, $format);
        } catch (\ValueError $e) {
            // On ne fait rien, ce qui nous intéresse sont
            // les exceptions de type DateWrongFormatException.
        } catch (DateWrongFormatException $e) {
            $isException = true;
        }

        return $isException;
    }

    private function now(): \DateTime
    {
        return new \DateTime();
    }

    private function createNowDateString(string $format): string
    {
        return $this->now()->format($format);
    }

    private function throwsException(string $dateValue, string $formatValue): bool
    {
        return $this->isException($this->createNowDateString($dateValue), $formatValue);
    }

    public function testValidDateFormat(): void
    {
        $this->assertFalse(
            $this->throwsException(
                DateUtilsInterface::FORMAT_SLASH_DDMMYYYY,
                DateUtilsInterface::FORMAT_SLASH_DDMMYYYY
            )
        );

        $this->assertFalse(
            $this->isException('12/02/1986', DateUtilsInterface::FORMAT_SLASH_DDMMYYYY)
        );

        $this->assertFalse(
            $this->isException('02/12/1986', DateUtilsInterface::FORMAT_SLASH_MMDDYYYY)
        );

        $this->assertFalse(
            $this->isException('1986-02-12', DateUtilsInterface::FORMAT_HYPHEN_YYYYMMDD)
        );
    }

    public function testWrongDateFormat(): void
    {
        $this->assertTrue(
            $this->throwsException(
                DateUtilsInterface::FORMAT_SLASH_DDMMYYYY,
                DateUtilsInterface::FORMAT_HYPHEN_YYYYMMDD
            )
        );
    }

    public function testTimestampEquals(): void
    {
        $date1 = $this->now();
        $date2 = DateUtils::stringToDatetime(
            $this->createNowDateString(DateUtilsInterface::FORMAT_SLASH_DDMMYYYY),
            DateUtilsInterface::FORMAT_SLASH_DDMMYYYY
        );

        $this->assertEquals($date1->getTimestamp(), $date2->getTimestamp());
    }
}
