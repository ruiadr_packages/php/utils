<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\SecurityUtils;

final class SecurityUtilsTest extends TestCase
{
    public function testRandomString(): void
    {
        $resultTest = SecurityUtils::randomString('abcd', 24);

        $this->assertEquals(24, strlen($resultTest));

        $resultTest = SecurityUtils::randomString('abcd', -10);

        $this->assertEquals(1, strlen($resultTest));
    }

    public function testRandomAlphanumWithoutUpper(): void
    {
        $resultTest = SecurityUtils::randomAlphanum(24);

        $this->assertEquals(24, strlen($resultTest));
    }

    public function testRandomAlphanumWithUpper(): void
    {
        $resultTest = SecurityUtils::randomAlphanum(24, true);

        $this->assertEquals(24, strlen($resultTest));
    }
}
