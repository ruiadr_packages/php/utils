<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\NumberUtils;

final class NumberUtilsTest extends TestCase
{
    public function testPercent1(): void
    {
        $this->assertSame(100.0, NumberUtils::percent(0.0, 100.0));
    }

    public function testPercent2(): void
    {
        $this->assertSame(50.0, NumberUtils::percent(50.0, 100.0));
    }

    public function testPercent3(): void
    {
        $this->assertSame(0.0, NumberUtils::percent(100.0, 100.0));
    }

    public function testPercent4(): void
    {
        $this->assertSame(-100.0, NumberUtils::percent(200.0, 100.0));
    }
}
