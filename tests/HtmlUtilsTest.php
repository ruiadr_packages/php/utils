<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\HtmlUtils;

final class HtmlUtilsTest extends TestCase
{
    private const HTML = '<html><h1>Hello World !</h1> <p>John Doe</p></html>';

    public function testSanitizeHTMLWithoutAllowedTags(): void
    {
        $expected = 'Hello World ! John Doe';

        $resultTest = HtmlUtils::sanitizeHTML(self::HTML);

        $this->assertSame($expected, $resultTest);
    }

    public function testSanitizeHTMLWithAllowedTags(): void
    {
        $expected = 'Hello World ! <p>John Doe</p>';

        $resultTest = HtmlUtils::sanitizeHTML(self::HTML, '<p>');

        $this->assertSame($expected, $resultTest);
    }
}
