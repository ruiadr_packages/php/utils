<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\StringUtils;

final class StringUtilsTest extends TestCase
{
    private const JOHN_DOE_1 = ' john Doe';
    private const JOHN_DOE_2 = 'jOhN  Doe ';
    private const JOHN_DOE_3 = ' JohN  DoE ';

    public function testLowerTrim(): void
    {
        $this->assertSame('john doe', StringUtils::lowerTrim(self::JOHN_DOE_1));
        $this->assertSame('john  doe', StringUtils::lowerTrim(self::JOHN_DOE_2));
        $this->assertSame('john  doe', StringUtils::lowerTrim(self::JOHN_DOE_3));
    }

    public function testUpperTrim(): void
    {
        $this->assertSame('JOHN DOE', StringUtils::upperTrim(self::JOHN_DOE_1));
        $this->assertSame('JOHN  DOE', StringUtils::upperTrim(self::JOHN_DOE_2));
        $this->assertSame('JOHN  DOE', StringUtils::upperTrim(self::JOHN_DOE_3));
    }

    public function testCapitalize(): void
    {
        $this->assertSame('John doe', StringUtils::capitalize(self::JOHN_DOE_1));
        $this->assertSame('John  doe', StringUtils::capitalize(self::JOHN_DOE_2));
        $this->assertSame('John  doe', StringUtils::capitalize(self::JOHN_DOE_3));
    }

    public function testSanitizeAlnum(): void
    {
        $this->assertSame('ac8DwX', StringUtils::sanitizeAlnum('é a _ ç c 8, DwX ^'));
        $this->assertSame('cd777x', StringUtils::sanitizeAlnum("/cd 7\nè77^{ x ;  "));
    }

    public function testReplaceSpecialChars(): void
    {
        $expected = 'aaaeeeeiioouuuycAAAEEEEIIOOUUUC';

        $resultTest = StringUtils::replaceSpecialChars('àâäéèêëîïôöùûüÿçÂÄÀÊËÉÈÎÏÔÖÛÜÙÇ');

        $this->assertSame($expected, $resultTest);
    }

    public function testShuffle(): void
    {
        $str = 'helloworld';

        $this->assertNotSame($str, StringUtils::shuffle($str));
        $this->assertNotSame($str, StringUtils::shuffle($str, true));
    }
}
