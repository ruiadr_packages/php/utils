<?php

namespace Ruiadr\Utils\Tests;

use PHPUnit\Framework\TestCase;
use Ruiadr\Utils\ArrayUtils;

final class ArrayUtilsTest extends TestCase
{
    public function testSimpleWithoutPreserveKey(): void
    {
        $a1 = ['a', 'b', 'c'];
        $a2 = ['d'];

        $resultTest = ArrayUtils::deepMergeArray([$a1, $a2]);
        $expected = ['a', 'b', 'c', 'd'];

        $this->assertSame($expected, $resultTest);
    }

    public function testSimpleWithPreserveKey(): void
    {
        $a1 = ['a', 'b', 'c'];
        $a2 = ['d'];

        $resultTest = ArrayUtils::deepMergeArray([$a1, $a2], true);
        $expected = ['d', 'b', 'c'];

        $this->assertSame($expected, $resultTest);
    }

    public function testSimpleWithKey(): void
    {
        $a1 = ['a' => 1, 'b' => 3, 'c' => 3];
        $a2 = ['b' => 2];

        $resultTest = ArrayUtils::deepMergeArray([$a1, $a2]);
        $expected = ['a' => 1, 'b' => 2, 'c' => 3];

        $this->assertSame($expected, $resultTest);
    }

    public function testWithDeepArrayMethod(): void
    {
        $a1 = ['a' => 1, 'b' => ['aa' => 1, 'bb' => 3], 'c' => 3];
        $a2 = ['b' => ['aa' => 1, 'bb' => 2], 'd' => 4];

        $resultTest = ArrayUtils::deepMergeArray([$a1, $a2]);
        $expected = ['a' => 1, 'b' => ['aa' => 1, 'bb' => 2], 'c' => 3, 'd' => 4];

        $this->assertSame($expected, $resultTest);
    }

    public function testWithDeepMethod(): void
    {
        $a1 = ['a' => 1, 'b' => ['aa' => 1, 'bb' => 3], 'c' => 3];
        $a2 = ['b' => ['aa' => 1, 'bb' => 2]];
        $a3 = ['d' => 4];

        $resultTest = ArrayUtils::deepMerge($a1, $a2, $a3);
        $expected = ['a' => 1, 'b' => ['aa' => 1, 'bb' => 2], 'c' => 3, 'd' => 4];

        $this->assertSame($expected, $resultTest);
    }
}
